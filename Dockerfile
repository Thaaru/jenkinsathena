# FROM node:18.13-alpine

# WORKDIR /app

# COPY . .

# RUN npm install

# CMD ["npm", "run", "start", "web"]

FROM node:18.13-alpine as build
WORKDIR /app
COPY ./package.json .
RUN npm install  --legacy-peer-deps
COPY . .
RUN npm run build
COPY ./.env.development.local /app/dist/apps/user-service 
RUN npm uninstall
CMD ["node","/app/dist/apps/user-service/main.js"]

# FROM nginx:alpine
# COPY ./conf.d /etc/nginx/conf.d
# COPY --from=build /app/dist/apps/web /usr/share/nginx/html
# EXPOSE 4200
# CMD ["nginx", "-g", "daemon off;"]


