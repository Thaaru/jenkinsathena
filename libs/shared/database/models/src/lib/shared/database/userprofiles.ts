import { Model, DataTypes, Optional } from 'sequelize'
import * as Sequelize from 'sequelize';


export interface userProfilesAttributes {

    id:number
    profile_image?: Blob,
    notification_preference?: string,
    address?: string,
    bio?: string,
    years_of_experience?: number;
    experience_updated_at?: Date;
}

export type userProfilesPk = "id";
export type userProfilesId = UserProfiles[userProfilesPk];
export type userprofilesOptinalAttributes = "id" | "profile_image" | "notification_preference" | "address" | "bio" | "years_of_experience" |"experience_updated_at"
export type userprofilesCreationAttributes = Optional<userProfilesAttributes, userprofilesOptinalAttributes>

export class UserProfiles extends Model implements userProfilesAttributes {

    id!: number
    profile_image?: Blob
    notification_preference?: string
    address?: string
    bio?: string
    years_of_experience?: number;
    experience_updated_at?: Date;



    static initModel(sequelize: Sequelize.Sequelize): typeof UserProfiles {
        return UserProfiles.init({

            id: {
                autoIncrement: true,
                type: DataTypes.BIGINT,
                allowNull: false,
                primaryKey: true
            },
            profile_image: {
                type: DataTypes.BLOB,
                allowNull: true,
                
            },
            notification_preference: {
                type: DataTypes.TEXT,
                allowNull: true,
            },
            address: {
                type: DataTypes.STRING,
                allowNull: true,
            },
            bio: {
                type: DataTypes.STRING,
                allowNull: true,
            },
            years_of_experience: {
                type: DataTypes.INTEGER,
                allowNull: true
            },
            experience_updated_at: {
                type: DataTypes.DATE,
                allowNull: true
            }

        },
            {
                sequelize,
                tableName: 'user_profiles',
                schema: 'public',
                timestamps: true,
                paranoid: true,

                indexes: [
                    {
                        name: "userprofiles_pkey",
                        unique: true,
                        fields: [
                            { name: "id" },
                        ]


                    }
                ]
            })
    }
}