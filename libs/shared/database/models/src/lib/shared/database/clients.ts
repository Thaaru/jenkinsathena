import * as Sequelize from "sequelize"
import { Model, DataTypes, Optional } from 'sequelize';


export interface clientsAttributes {
    id: number,
    name: string,
    website?: string,
    logo?: Blob,
    address?: string,
    state?: string,
    city?: string,
    country?: string,
    email?: string,
    pincode?: string,
    phone_number?: string
}

export type clientsPk = "id";
export type clientsId = Clients[clientsPk];
export type clientsOptionalAttributes ="id"|"website" |"logo"|"address"|"state"|"country"|"city"|"email"|"pincode"|"phone_number"
export type clientsCreationAttributes = Optional<clientsAttributes,clientsOptionalAttributes>


export class Clients extends Model implements clientsAttributes {
    id!:number
    name!: string
    website?: string
    logo?: Blob
    address?: string
    state?: string
    city?: string
    country?: string
    email?: string
    pincode?: string
    phone_number?: string

    static initModel(sequelize: Sequelize.Sequelize): typeof Clients {
        return Clients.init({
            id: {
                autoIncrement: true,
                type: DataTypes.BIGINT,
                allowNull: false,
                primaryKey: true
            },
            name: {
                type: DataTypes.STRING,
                allowNull: false,
                unique: true,
            },
            website: {
                type: DataTypes.STRING,
                allowNull: true,
            },
            logo: {
                type: DataTypes.BLOB,
                allowNull: true,
            },
            address: {
                type: DataTypes.STRING,
                allowNull: true,
            },
            state: {
                type: DataTypes.STRING,
                allowNull: true,
            },
            city: {
                type: DataTypes.STRING,
                allowNull: true,
            },
            country: {
                type: DataTypes.STRING,
                allowNull: true,
            },
            email: {
                type: DataTypes.STRING,
                allowNull: true,
            },
            pincode: {
                type: DataTypes.STRING,
                allowNull: true,
            },
            phone_number: {
                type: DataTypes.STRING,
                allowNull: true,
            }
        }, {
            sequelize,
            tableName: 'clients',
            schema: 'public',
            timestamps: true,
            paranoid: true,

            indexes: [
                {
                    name: "client_pkey",
                    unique: true,
                    fields: [
                        { name: "id" },
                    ]
                }
            ]
        })
    }
}