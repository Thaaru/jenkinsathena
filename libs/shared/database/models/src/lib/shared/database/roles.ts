import * as sequelize from 'sequelize'
import { Model, DataTypes } from 'sequelize'
export interface rolesAttributes {

    id: number
    role_name: string,
    permission: string
}


export type rolesPk = "id";
export type rolesId = Roles[rolesPk];
export type rolesCreationAttributes = rolesAttributes; 


export class Roles extends Model implements rolesAttributes {
    id!: number
    role_name!: string
    permission!: string


    static initModel(sequelize: sequelize.Sequelize): typeof Roles {

        return Roles.init({
            id: {
                autoIncrement: true,
                type: DataTypes.BIGINT,
                allowNull: false,
                primaryKey: true
            },
            role_name: {
                type: DataTypes.STRING,
                allowNull: false
            },
            permission: {
                type: DataTypes.TEXT,
                allowNull: false,

            }
        }, {
            sequelize,
            tableName: "roles",
            schema: 'public',
            timestamps: true,
            paranoid: true,

            indexes: [
                {
                    name: "roles_pkey",
                    unique: true,
                    fields: [
                        { name: "id" },
                    ]


                }
            ]
        }
        )
    }
}