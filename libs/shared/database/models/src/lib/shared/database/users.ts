import * as Sequelize from 'sequelize';
import { DataTypes, Model, Optional, NonAttribute } from 'sequelize';
import type { Roles, rolesId } from './roles';


export interface usersAttributes {
    id: number;
    handle: string;
    phone_number?: string;
    //name: string;
    provider?: string;
    uuid?: string;
    email: string;
    personal_email?:string;
    //encrypted_password: string;
    // reset_password_token?: string;
    // reset_password_sent_at?: Date;
    // remember_created_at?: Date;
    // confirmation_token?: string;
    // confirmed_at?: Date;
    // confirmation_sent_at?: Date;
    // unconfirmed_email?: string;
    // accepted_privacy_policy_at?: Date;
    // accepted_terms_at?: Date;
    // became_mentor_at?: Date;
     deleted_at?: Date;
    // joined_research_at?: Date;
    // github_username?: string;
    // reputation: number;
    // bio?: string;
    // avatar_url?: string;
    // location?: string;
    // pronouns?: string;
    // num_solutions_mentored: number;
    // mentor_satisfaction_percentage?: number;
    // stripe_customer_id?: string;
    // total_donated_in_cents?: number;
    // active_donation_subscription?: boolean;
    created_at: Date;
    updated_at: Date;
    // show_on_supporters_page: boolean;
    // client_id?: number;
    is_active?: boolean;
    users_type?: string;
    alternative_phoneNumber: string;
    email_confirmed?: boolean;
    first_name: string;
    last_name: string;
    work_email?: string;
    created_by: number;
    roles?: number[];
    

   
    
}


export type usersPk = "id";
export type usersId = Users[usersPk];
export type usersOptionalAttributes = "id"| "provider" | "uuid" | "email" | "deleted_at" | "created_at" | "updated_at" ;
export type usersCreationAttributes = Optional<usersAttributes, usersOptionalAttributes>;

export class Users extends Model implements usersAttributes {

    users() {
        console.log("user constructor callerd");
    }

    id!: number;
    handle!: string;
   // name!: string;
    provider?: string;
    phone_number?: string;
    uuid?: string;
    email!: string;
    // encrypted_password!: string;
    // reset_password_token?: string;
    // reset_password_sent_at?: Date;
    // remember_created_at?: Date;
    // confirmation_token?: string;
    // confirmed_at?: Date;
    // confirmation_sent_at?: Date;
    // unconfirmed_email?: string;
    // accepted_privacy_policy_at?: Date;
    // accepted_terms_at?: Date;
    // became_mentor_at?: Date;
     deleted_at?: Date;
    // joined_research_at?: Date;
    // github_username?: string;
    // reputation!: number;
    // bio?: string;
    // avatar_url?: string;
    // location?: string;
    // pronouns?: string;
    // num_solutions_mentored!: number;
    // mentor_satisfaction_percentage?: number;
    // stripe_customer_id?: string;
    // total_donated_in_cents?: number;
    // active_donation_subscription?: boolean;
    created_at!: Date;
    updated_at!: Date;
    // show_on_supporters_page!: boolean;
    // client_id?: number;
    is_active?: boolean;
    roles?: number[];
    users_type?: string;
    alternative_phoneNumber: string;
    personal_email?: string;
    email_confirmed?: boolean;
    first_name!: string;
    last_name!: string;
    work_email?: string;
    created_by: number;


    Roles!: Roles[];
    getUser_roles!: Sequelize.BelongsToManyGetAssociationsMixin<Roles>
    setUser_roles!: Sequelize.BelongsToManySetAssociationsMixin<Roles, number>
    addUser_roles!: Sequelize.BelongsToManyAddAssociationMixin<Roles, rolesId>
    createUser_roles!: Sequelize.BelongsToManyCreateAssociationMixin<Roles>



    static initModel(sequelize: Sequelize.Sequelize): typeof Users {

        return Users.init({
            id: {
                autoIncrement: true,
                type: DataTypes.BIGINT,
                allowNull: false,
                primaryKey: true
            },
            phone_number: {
                type: DataTypes.STRING,
                allowNull: true
            },
            handle: {
                type: DataTypes.STRING,
                allowNull: false
            },
            // name: {
            //     type: DataTypes.STRING,
            //     allowNull: false
            // },
            provider: {
                type: DataTypes.STRING,
                allowNull: true
            },
            uuid: {
                type: DataTypes.STRING,
                allowNull: true
            },
            email: {
                type: DataTypes.STRING,
                allowNull: false,
                defaultValue: ""
            },
            // encrypted_password: {
            //     type: DataTypes.STRING,
            //     allowNull: false,
            //     defaultValue: ""
            // },
            // reset_password_token: {
            //     type: DataTypes.STRING,
            //     allowNull: true
            // },
            // reset_password_sent_at: {
            //     type: DataTypes.DATE,
            //     allowNull: true
            // },
            // remember_created_at: {
            //     type: DataTypes.DATE,
            //     allowNull: true
            // },
            // confirmation_token: {
            //     type: DataTypes.STRING,
            //     allowNull: true
            // },
            // confirmed_at: {
            //     type: DataTypes.DATE,
            //     allowNull: true
            // },
            // confirmation_sent_at: {
            //     type: DataTypes.DATE,
            //     allowNull: true
            // },
            // unconfirmed_email: {
            //     type: DataTypes.STRING,
            //     allowNull: true
            // },
            // accepted_privacy_policy_at: {
            //     type: DataTypes.DATE,
            //     allowNull: true
            // },
            // accepted_terms_at: {
            //     type: DataTypes.DATE,
            //     allowNull: true
            // },
            // became_mentor_at: {
            //     type: DataTypes.DATE,
            //     allowNull: true
            // },
            // joined_research_at: {
            //     type: DataTypes.DATE,
            //     allowNull: true
            // },
            // github_username: {
            //     type: DataTypes.STRING,
            //     allowNull: true
            // },
            // reputation: {
            //     type: DataTypes.INTEGER,
            //     allowNull: false,
            //     defaultValue: 0
            // },
            // bio: {
            //     type: DataTypes.TEXT,
            //     allowNull: true
            // },
            // avatar_url: {
            //     type: DataTypes.STRING,
            //     allowNull: true
            // },
            // location: {
            //     type: DataTypes.STRING,
            //     allowNull: true
            // },
            // pronouns: {
            //     type: DataTypes.STRING,
            //     allowNull: true
            // },
            // num_solutions_mentored: {
            //     type: DataTypes.INTEGER,
            //     allowNull: false,
            //     defaultValue: 0
            // },
            // mentor_satisfaction_percentage: {
            //     type: DataTypes.SMALLINT,
            //     allowNull: true
            // },
            // stripe_customer_id: {
            //     type: DataTypes.STRING,
            //     allowNull: true
            // },
            // total_donated_in_cents: {
            //     type: DataTypes.INTEGER,
            //     allowNull: true,
            //     defaultValue: 0
            // },
            // active_donation_subscription: {
            //     type: DataTypes.BOOLEAN,
            //     allowNull: true,
            //     defaultValue: false
            // },
            // show_on_supporters_page: {
            //     type: DataTypes.BOOLEAN,
            //     allowNull: false,
            //     defaultValue: true
            // },
            // client_id: {
            //     type: DataTypes.INTEGER,
            //     allowNull: true
            // },
            roles: {
                type: DataTypes.TEXT,
                allowNull: true
            },
            is_active: {
                type: DataTypes.BOOLEAN,
                allowNull: true,
                defaultValue: true
            },
            users_type: {
                type: DataTypes.STRING,
                allowNull: true,
            },
            alternative_phoneNumber: {
                type: DataTypes.STRING,
                allowNull: true,
            },
            personal_email: {
                type: DataTypes.STRING,
                allowNull: true,
            },
            email_confirmed: {
                type: DataTypes.BOOLEAN,
                allowNull: true,
            },
            first_name: {
                type: DataTypes.STRING,
                allowNull: false,  
            },
            last_name: {
                type: DataTypes.STRING,
                allowNull: false,
            },
            work_email: {
                type: DataTypes.STRING,
                allowNull: true
            },
            created_by: {
                type: DataTypes.BIGINT,
                allowNull: false
            }

        }, {
            sequelize,
            tableName: 'users',
            schema: 'public',
            timestamps: true,
            paranoid: true,
            defaultScope: {
                attributes: { exclude: ['encrypted_password', 'reset_password_token', 'confirmation_token', 'stripe_customer_id'] }
            },
            indexes: [
                // {
                //     name: "index_users_on_confirmation_token",
                //     unique: true,
                //     fields: [
                //         { name: "confirmation_token" },
                //     ]
                // },
                {
                    name: "index_users_on_email",
                    unique: true,
                    fields: [
                        { name: "email" },
                    ]
                },
                // {
                //     name: "index_users_on_github_username",
                //     unique: true,
                //     fields: [
                //         { name: "github_username" },
                //     ]
                // },
                {
                    name: "index_users_on_handle",
                    unique: true,
                    fields: [
                        { name: "handle" },
                    ]
                },
                {
                    name: "index_users_on_provider_and_uuid",
                    unique: true,
                    fields: [
                        { name: "provider" },
                        { name: "uuid" },
                    ]
                },
                // {
                //     name: "index_users_on_reset_password_token",
                //     unique: true,
                //     fields: [
                //         { name: "reset_password_token" },
                //     ]
                // },
                // {
                //     name: "index_users_on_stripe_customer_id",
                //     unique: true,
                //     fields: [
                //         { name: "stripe_customer_id" },
                //     ]
                // },
                // {
                //     name: "index_users_on_unconfirmed_email",
                //     fields: [
                //         { name: "unconfirmed_email" },
                //     ]
                // },
                // {
                //     name: "users-supporters-page",
                //     fields: [
                //         { name: "total_donated_in_cents" },
                //         { name: "show_on_supporters_page" },
                //     ]
                // },
                {
                    name: "users_pkey",
                    unique: true,
                    fields: [
                        { name: "id" },
                    ]
                },
            ]
        });
    }
}


