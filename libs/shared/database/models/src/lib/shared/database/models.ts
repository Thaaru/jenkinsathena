import type { Sequelize } from 'sequelize';

import { Users as _users } from './users';
import type { usersAttributes, usersCreationAttributes } from './users';

import { Clients as _clients } from './clients';
import type { clientsAttributes, clientsCreationAttributes } from './clients';

import { UserProfiles as _userProfiles } from './userprofiles';
import type { userProfilesAttributes, userprofilesCreationAttributes } from './userprofiles';

import { Roles as _roles } from './roles';
import type { rolesCreationAttributes, rolesAttributes } from './roles';

export {
    _users as users,
    _clients as clients,
    _userProfiles as userProfiles,
    _roles as roles
}
export type {
    usersAttributes,
    usersCreationAttributes,
    clientsAttributes,
    clientsCreationAttributes,
    userProfilesAttributes,
    userprofilesCreationAttributes,
    rolesCreationAttributes,
    rolesAttributes


}

export function initModels(sequelize: Sequelize) { 
    const users = _users.initModel(sequelize);
    const clients = _clients.initModel(sequelize);
    const userProfiles = _userProfiles.initModel(sequelize);
    const roles = _roles.initModel(sequelize);


    users.hasOne(userProfiles, {
        as: 'userProfiles',
        foreignKey: 'user_id',
    });
    userProfiles.belongsTo(users, { as: 'user', foreignKey: 'user_id' });

    users.belongsTo(clients, { as: 'client', foreignKey: 'client_id' });
    clients.hasMany(users, { as: 'client_users', foreignKey: 'client_id' });

    users.belongsToMany(roles, {
        as: 'user_roles',
        through: 'userroles',
    });

    return {
        users: users,
        clients: clients,
        userProfiles: userProfiles,
        roles: roles

    }
}

