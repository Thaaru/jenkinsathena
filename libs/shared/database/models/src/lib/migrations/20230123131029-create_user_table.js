'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('users', {
      id: {
        autoIncrement: true,
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true
      },
      handle: {
        type: Sequelize.STRING,
        allowNull: false
      },
    }, {
      Sequelize,
      tableName: 'users',
      schema: 'public',
      timestamps: true,
      paranoid: true,
    }
    )
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.dropTable('users')
  }
};
