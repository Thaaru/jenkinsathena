// export * from './lib/shared\\database\\models';

import { Sequelize } from 'sequelize';
import { NODE_ENV, DB_HOST, DB_PORT, DB_USER, DB_PASSWORD, DB_DATABASE } from './config/index';
//export const { NODE_ENV, PORT, DB_HOST, DB_PORT, DB_USER, DB_PASSWORD, DB_DATABASE, SECRET_KEY, LOG_FORMAT, LOG_DIR, ORIGIN, SUPABASE_URL, SUPABASE_ANON_KEY,GITHUB_TOKEN,GIT_URL,GIT_LOCAL_PATH,REDIS_HOST,REDIS_PORT,REDIS_DATABASE } = process.env;
import { initModels } from './lib/shared/database/models'
console.log("password checking", DB_PASSWORD, DB_USER, DB_DATABASE, DB_HOST);
const sequelize = new Sequelize(DB_DATABASE ? DB_DATABASE : "", DB_USER ? DB_USER : "", DB_PASSWORD, {
   
    dialect: 'postgres',
    host: DB_HOST,
    port: Number(DB_PORT),
    define: {
        charset: 'unicode',
        underscored: true,
        freezeTableName: true,
    },
    pool: {
        min: 0,
        max: 10,
    },
    logQueryParameters: NODE_ENV === 'development',
    logging: false,
    benchmark: true,
});


sequelize.authenticate();

sequelize.sync();
const DB = {
    Users: initModels(sequelize).users,
    // Tracks: new tracks(sequelize),
    DBmodels: initModels(sequelize),
    sequelize, // connection instance (RAW queries)
    Sequelize, // library
};


export default DB;
