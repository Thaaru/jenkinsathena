# shared-database-models

This library was generated with [Nx](https://nx.dev).

## Running unit tests

Run `nx test shared-database-models` to execute the unit tests via [Jest](https://jestjs.io).

## Running lint

Run `nx lint shared-database-models` to execute the lint via [ESLint](https://eslint.org/).
