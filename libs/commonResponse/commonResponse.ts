export const responseCF = (body:any) => {

    return !body?{
        "body":null,
    }:{
      
       "body":body
    }
  }
  

  
  export const bodyCF = ({ val, err,code,status }:{val?:any,err?:any,code?:string,status?:boolean}):any => {
    return {
      "code": !code ? null : code,
      "value": !val ? null : val,
      "status":!status ? null :status,
      "error": !err ? null : err

    }
  }
  
  
  