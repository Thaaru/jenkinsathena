import './ButtonComponent.css';
import { Col, Container, Row } from 'react-bootstrap';
import Button from 'react-bootstrap/Button';

export function ButtonComponent(props) {
  return (
    <Container>
      <Row className='d-flex align-items-center justify-content-center mb-2'>
          <Col md="auto d-flex flex-wrap align-items-center justify-content-center justify-content-md-between p-2 gap-1 h-3">
            <Button variant = "outline-dark" className='height'>Delete</Button> &nbsp;
            <Button variant = "dark" className='height'>Disable</Button> &nbsp;
            <Button className='Adduserbutton py-1 text'>
              + Add User
            </Button>
          </Col>
      </Row>
    </Container>
  );
}
export default ButtonComponent;
