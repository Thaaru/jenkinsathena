import './Layout.css';
import { Box } from '@athena-v2/web/ui';
import { Sidebar } from '@athena-v2/web/components';

export function Layout(props) {
  return (
    <Box className="main-wrapper">
    <Box className="sidenav-wrapper">
      <Sidebar />
      </Box>
      <Box className="content-wrapper">{props.children}</Box>
    </Box>
  );
}
export default Layout;
