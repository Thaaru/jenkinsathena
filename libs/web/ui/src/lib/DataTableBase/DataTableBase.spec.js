import { render } from '@testing-library/react';
import DataTableBase from './DataTableBase';
describe('DataTableBase', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<DataTableBase />);
    expect(baseElement).toBeTruthy();
  });
});
