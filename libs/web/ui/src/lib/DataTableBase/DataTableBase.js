// import './DataTableBase.css';
import React from 'react';
import DataTable from 'react-data-table-component';
import { SortingIcon,EditIcon,DeleteIcon,FilterIcon } from '../Icons/Icons';

export function DataTableBase() {

  const columns = [
    {
      name: '',
      selector: row => (<img width={30} height={30} src='https://www.teahub.io/photos/full/311-3116253_boys-pics-for-dp-handsome-boys-dp.jpg' alt='pic'/>),
    },
    {
      name: 'Full Name',
      selector: row => row.fullname,
      sortable: true,

    },
    {
      name: 'Email ID',
      selector: row => row.emailId,
      sortable: true,
    },
    {
      name: 'Registration Type',
      selector: row => row.registrationType,
      sortable:true,
    },
    {
      name: 'Roles',
      selector: row => row.roles,
      sortable: true,
    },
    {
      name: 'Action',
      selector: row => row.Action,
    },
  ];

  const data = [
    {
      id: 1,
      // profilePic: 'https://www.teahub.io/photos/full/311-3116253_boys-pics-for-dp-handsome-boys-dp.jpg',
      fullname: 'Javed Khan',
      emailId: 'javed@gmail.com',
      registrationType: 'Self Registered',
      Action: <><EditIcon /><DeleteIcon /></>,


    },
    {
      id: 2,
      fullname: 'Anu Malik',
      emailId: 'anu@gmail.com',
      registrationType: 'Platform Registered',
      Action: '',


    },
    {
      id: 3,
      fullname: 'Sachin Devakan',
      emailId: 'Sachin@gmail.com',
      registrationType: 'Self Registered',
      Action: '',


    },
    {
      id: 4,
      fullname: 'Valli Vedanayakam',
      emailId: 'valli@gmail.com',
      registrationType: 'Platform Registered',
      Action: '',


    }, {
      id: 5,
      fullname: 'Anu Malik',
      emailId: 'anu@gmail.com',
      registrationType: 'Platform Registered',
      Action: '',


    },
    {
      id: 6,
      fullname: 'Sachin Devakan',
      emailId: 'Sachin@gmail.com',
      registrationType: 'Self Registered',
      Action: '',

    },
    {
      id: 7,
      fullname: 'Sachin Devakan',
      emailId: 'Sachin@gmail.com',
      registrationType: 'Self Registered',
      Action: '',


    },
    {
      id: 8,
      fullname: 'Valli Vedanayakam',
      emailId: 'valli@gmail.com',
      registrationType: 'Self Registered',
      Action: '',


    }, {
      id: 9,
      fullname: 'Anu Malik',
      emailId: 'anu@gmail.com',
      registrationType: 'Platform Registered',
      Action: '',


    },
    {
      id: 10,
      fullname: 'Sachin Devakan',
      emailId: 'Sachin@gmail.com',
      registrationType: 'Self Registered',
      Action: '',

    },
    {
      id: 11,
      fullname: 'Anu Malik',
      emailId: 'anu@gmail.com',
      registrationType: 'Platform Registered',
      Action: '',


    },
    {
      id: 12,
      fullname: 'Sachin Devakan',
      emailId: 'Sachin@gmail.com',
      registrationType: 'Self Registered',
      Action: '',


    },
    {
      id: 13,
      fullname: 'Valli Vedanayakam',
      emailId: 'valli@gmail.com',
      registrationType: 'Self Registered',
      Action: '',


    }, {
      id: 14,
      fullname: 'Anu Malik',
      emailId: 'anu@gmail.com',
      registrationType: 'Platform Registered',
      Action: '',


    },
    {
      id: 15,
      fullname: 'Sachin Devakan',
      emailId: 'Sachin@gmail.com',
      registrationType: 'Self Registered',
      Action: '',

    },
    // {
    //   id: 16,
    //   fullname: 'Sachin Devakan',
    //   emailId: 'Sachin@gmail.com',
    //   registrationType: 'Self Registered',
    //   Action: '',


    // },
    // {
    //   id: 17,
    //   fullname: 'Valli Vedanayakam',
    //   emailId: 'valli@gmail.com',
    //   registrationType: 'Platform Registered',
    //   Action: '',


    // }, {
    //   id: 19,
    //   fullname: 'Anu Malik',
    //   emailId: 'anu@gmail.com',
    //   registrationType: 'Platform Registered',
    //   Action: '',


    // },
    // {
    //   id: 20,
    //   fullname: 'Sachin Devakan',
    //   emailId: 'Sachin@gmail.com',
    //   registrationType: 'Self Registered',
    //   Action: '',

    // }
  ]
  console.log("......", data);

  return (
    <DataTable className="dataTable-content"
    // title="Learners"
    columns={columns}
    data={data}
    direction="auto"
    fixedHeaderScrollHeight="300px"
    pagination
    responsive
    selectableRows
    selectableRowsHighlight
    subHeaderAlign="right"
    subHeaderWrap
    sortIcon={<SortingIcon />}
    />
  )
}
export default DataTableBase;
