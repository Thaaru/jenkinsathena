import './BreadcrumbsComponent.css';
import Breadcrumb from 'react-bootstrap/Breadcrumb';

export function BreadcrumbsComponent(props) {
  return (
    <div>
      <Breadcrumb className='d-flex flex-wrap'>
          <Breadcrumb.Item active>Dashboard</Breadcrumb.Item>
          <Breadcrumb.Item>Users</Breadcrumb.Item>
      </Breadcrumb>
      <p className='fw-bold title d-flex'>Users</p> 
    </div>       
  );
}
export default BreadcrumbsComponent;
