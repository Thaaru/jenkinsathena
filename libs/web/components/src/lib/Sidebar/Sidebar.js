import './Sidebar.css';
import Nav from 'react-bootstrap/Nav';
import Button from 'react-bootstrap/Button';
import { NavLink } from 'react-router-dom';
import Collapse from 'react-bootstrap/Collapse';
import { useState } from 'react';
import {
  DashboardIcon,
  AthenaLogo,
  PeopleIcon,
  Box,
  Avatar,
  DownArrow,
  Span,
} from '@athena-v2/web/ui';

export function Sidebar(props) {
  const items = [
    {
      id: 1,
      title: 'Dashboard',
    },
    {
      id: 2,
      title: 'Users',
      subtitle: [
        {
          title: 'Manage Users',
          to: '/manageuser',
        },
        {
          title: 'Add User',
          to: '/adduser',
        },
      ],
    },
    {
      id: 3,
      title: 'Skills',
    },
    {
      id: 4,
      title: 'Clients',
    },
    {
      id: 5,
      title: 'Reports',
    },
    {
      id: 6,
      title: 'Profile',
    },
  ];
  const [open, setOpen] = useState({});

  const handleClick = (id) => {
    setOpen((prevState) => ({ ...prevState, [id]: !prevState[id] }));
  };

  return (
    <Nav
      as="ul"
      className="col-lg-12 d-none d-lg-block sidebar bg-white"
      activeKey="/home"
    >
      <Box className="athena-logo">
        <AthenaLogo />
      </Box>
      {items.map((e, index) => (
        <Nav.Item as="li" >
          <Nav.Link>
            <Span className="d-flex justify-content-between align-items-center">
              <Span>
                {e.title == 'Dashboard' ? <DashboardIcon /> : <PeopleIcon />}
                &nbsp;&nbsp;&nbsp;{e.title}
              </Span>
              <i
                onClick={() => handleClick(e.id)}
                aria-expanded={open}
                aria-controls="collapseID"
                data-target={`#collapseID${index}`}
              >
                {e.title == 'Users' && <DownArrow />}
              </i>
            </Span>
          </Nav.Link>

          <Collapse in={open[e.id]}>
            <Box
              key={e.id}
              id="example-collapse-text"
              className="collapsedText"
            >
              <ul>
                {e.subtitle?.map((d, index) => (
                  <li className="mb-3 mt-2">
                    <NavLink
                      className="sub-links"
                      exact
                      to={d.to}
                      // isActive={() => [d.to].includes(pathname)}
                    >
                      -- {d.title}
                    </NavLink>
                  </li>
                ))}
              </ul>
            </Box>
          </Collapse>
        </Nav.Item>
      ))}
      <Box className="side-foot">
        <Box>
          <Avatar src="assets/images/Ellipse 1.png" />
        </Box>
        <h4>Aani Arputharaj</h4>
        <h6 className="text-secondary">Administrator</h6>
        <Button variant="primary" size="sm" className="rounded-0 mprofie-btn">
          Manage Profile
        </Button>
      </Box>
    </Nav>
  );
}
export default Sidebar;
