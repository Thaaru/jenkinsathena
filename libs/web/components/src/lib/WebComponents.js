import styles from './WebComponents.module.css';
export function WebComponents(props) {
  return (
    <div className={styles['container']}>
      <h1>Welcome to WebComponents!</h1>
    </div>
  );
}
export default WebComponents;
