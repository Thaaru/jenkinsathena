import { render } from '@testing-library/react';
import HeaderComponent from './HeaderComponents';
describe('HeaderComponent', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<HeaderComponent />);
    expect(baseElement).toBeTruthy();
  });
});
