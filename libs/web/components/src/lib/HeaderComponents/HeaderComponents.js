import './HeaderComponents.css';
import {Row, Container, Col, Navbar, Nav} from 'react-bootstrap'
import { BreadcrumbsComponent, ButtonComponent } from '@athena-v2/web/ui';

export function HeaderComponents(props) {
  return (
    <Navbar className="header d-flex me-2 justify-content-end bg-white p-3 " 
    bg="light" 
    expand="lg">
      <Container className="headernav">
            <Nav>
              <BreadcrumbsComponent/>
            </Nav>
            <Navbar className='justify-content-end'>
              <ButtonComponent/>
            </Navbar>
      </Container>
    </Navbar>
     );
}
export default HeaderComponents;
