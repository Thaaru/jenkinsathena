import { render } from '@testing-library/react';
import WebComponents from './WebComponents';
describe('WebComponents', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<WebComponents />);
    expect(baseElement).toBeTruthy();
  });
});
