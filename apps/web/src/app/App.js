import './App.css';
import { Route, Routes, Link } from 'react-router-dom';
import {
  WebUi,
  Layout,
  DataTableBase,
} from '@athena-v2/web/ui';
import {
  WebComponents,
  HeaderComponents,
  Sidebar,
} from '@athena-v2/web/components';

export function App() {
  return (
    <Layout>
          <HeaderComponents />
      <DataTableBase />
    </Layout>
  );
}
export default App;
