import App from '@/serverApp';
import UsersRoute from '@routes/users.route'

const routes = [];
routes.push(new UsersRoute());

const app = new App(routes);
app.listen()