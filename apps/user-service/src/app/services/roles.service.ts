import { hash } from 'bcrypt';
import DB from "../../../../../libs/shared/database/models/src/index";
import { CreateRolesDto } from '@dtos/roles.dto';
import { HttpException } from '../../../../../libs/shared/exceptions/src/index';
import { Roles } from '@interfaces/roles.interface';
import { isEmpty } from '@utils/util';

class RolesService {
    public roles = DB.DBmodels.roles;

    public async findAllRole(): Promise<Roles[]> {
        const allRole: Roles[] = await this.roles.findAll();
        return allRole;
    }

    public async findRoleById(roleId: number): Promise<Roles> {
        if (isEmpty(roleId)) throw new HttpException(400, "RoleId is empty");

        const findRole: Roles = await this.roles.findByPk(roleId);
        if (!findRole) throw new HttpException(409, "Role doesn't exist");

        return findRole;
    }

    public async createRole(roleData: CreateRolesDto): Promise<Roles> {
        if (isEmpty(roleData)) throw new HttpException(400, "Role Data is empty");

        const findRole: Roles = await this.roles.findOne({ where: { role_name: roleData.role_name } });
        if (findRole) throw new HttpException(409, `This role ${roleData.role_name} already exists`);
        const createRoleData: Roles = await this.roles.create({ ...roleData });

        return createRoleData;
    }

    public async updateRole(roleId: number, roleData: CreateRolesDto): Promise<Roles> {
        if (isEmpty(roleData)) throw new HttpException(400, "Role Data is empty");

        const findRole: Roles = await this.roles.findByPk(roleId);
        if (!findRole) throw new HttpException(409, "Role doesn't exist");

        const updateRole: Roles = await this.roles.findByPk(roleId);
        return updateRole;
    }

    public async deleteRole(roleId: number): Promise<Roles> {
        if (isEmpty(roleId)) throw new HttpException(400, "Role doesn't existId");

        const findRole: Roles = await this.roles.findByPk(roleId);
        if (!findRole) throw new HttpException(409, "Role doesn't exist");

        await this.roles.destroy({ where: { id: roleId } });

        return findRole;
    }
}

export default RolesService;
