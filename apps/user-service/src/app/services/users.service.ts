import DB from '../../../../../libs/shared/database/models/src/index';
import { User } from '@interfaces/users.interface';
import { isEmpty } from '@utils/util';
import { HttpException } from '../../../../../libs/shared/exceptions/src/index';
import { CreateUserDto } from '@dtos/users.dto';
import { hash } from 'bcrypt';


class UserService{
    public users = DB.DBmodels.users;
    

    public async findAllUser(): Promise<User[]> {
        const allUser: User[] = await this.users.findAll();
        return allUser;
    }

   
    public async createUser(userData: CreateUserDto): Promise<User> {
        console.log("here:::::::::::::::::::")
        if (isEmpty(userData)) throw new HttpException(400, 'userData is empty');

        const findUser = await this.users.findOne({ where: { email: userData.email } });
        if (findUser) throw new HttpException(409, `This email ${userData.email} already exists`);

        const hashedPassword = await hash(userData.password, 10);
        const createUserData: User = await this.users.create({ ...userData, password: hashedPassword });

        
        return createUserData;
    }

    public async updateUser(userId: number, userData: CreateUserDto): Promise<User> {
        if (isEmpty(userData)) throw new HttpException(400, 'userData is empty');
        const findUser = await this.users.findByPk(userId);
        if (!findUser) throw new HttpException(409, "User doesn't exist");

        await this.users.update(userData, { where: { id: userId } });
        if (userData.roles) {
            await findUser.setUser_roles(userData.roles);
        }
        const updatedUser = await this.users.findByPk(userId);
        return updatedUser;
    }

    public async deleteUser(userId: number): Promise<User> {
        if (isEmpty(userId)) throw new HttpException(400, "User doesn't exist");

        const findUser: User = await this.users.findByPk(userId);
        if (!findUser) throw new HttpException(409, "User doesn't exist");

        await this.users.destroy({ where: { id: userId } });

        return findUser;
    }

}

export default UserService;
