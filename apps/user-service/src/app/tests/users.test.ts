import { config } from 'dotenv';
config({ path: ".env.development.local" })
import request from 'supertest';
import App from '../../serverApp';
import UsersRoute from '@routes/users.route'


describe("testing the users route", () => {

    describe(`[GetAll] /api/users/`, () => {
        it("getting all users details", async () => {
            const usersRoute: UsersRoute = new UsersRoute();
            const app: App = new App([usersRoute]);
            const response: request.Response = await request(app.getServer())
                .get(`/api/${usersRoute.path}`)

            expect(response.statusCode).toBe(200)
        })

        it("internal error", async () => {
            const usersRoute: UsersRoute = new UsersRoute();
            const app: App = new App([usersRoute]);
            const response: request.Response = await request(app.getServer())
                .get(`/api/${usersRoute.path}`)

            expect(response.statusCode).toBe(500)
        })
    })

})