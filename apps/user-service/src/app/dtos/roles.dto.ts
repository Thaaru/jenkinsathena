import { IsString } from 'class-validator';

export class CreateRolesDto {
    @IsString()
    public role_name: string;

    @IsString()
    public permission: string;

} 