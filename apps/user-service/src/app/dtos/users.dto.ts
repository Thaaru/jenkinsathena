import { IsString, IsEmail, IsInt, IsArray } from 'class-validator';

export class CreateUserDto {
    @IsEmail()
    public email: string;

    @IsString()
    public password: string;

    @IsString()
    public uid: string;

    @IsString()
    public handle: string;

    @IsString()
    public name: string;

    @IsInt()
    public client_id?: number;

    @IsArray()
    public roles: number[];
}
