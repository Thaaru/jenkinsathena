import { NextFunction, Request, Response } from 'express';
import { CreateRolesDto } from '@dtos/roles.dto';
import { Roles } from '@interfaces/roles.interface';
import rolesService from '@services/roles.service';

class RolesController{
    public rolesService = new rolesService();


    public getRoles = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const findAllRolesData: Roles[] = await this.rolesService.findAllRole();

            res.status(200).json({ data: findAllRolesData, message: 'findAll', status: 'success' });
        } catch (error) {
            res.status(404).json({ message: error.message, code: error.status, status: "error" });
            next(error);
        }
    };

    public getRoleById = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const roleId = Number(req.params.id);
            const findOneRoleData: Roles = await this.rolesService.findRoleById(roleId);

            res.status(200).json({ data: findOneRoleData, message: 'findOne', status: 'success' });
        } catch (error) {
            res.status(404).json({ message: error.message, code: error.status, status: "error" });
            next(error);
        }
    };

    public createRole = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const roleData: CreateRolesDto = req.body;
            const createRoleData: Roles = await this.rolesService.createRole(roleData);

            res.status(201).json({ data: createRoleData, message: 'created', status: 'success' });
        } catch (error) {
            res.status(404).json({ message: error.message, code: error.status, status: "error" });
            next(error);
        }
    };

    public updateRole = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const roleId = Number(req.params.id);
            const roleData: CreateRolesDto = req.body;
            const updateRoleData: Roles = await this.rolesService.updateRole(roleId, roleData);

            res.status(200).json({ data: updateRoleData, message: 'updated', status: 'success' });
        } catch (error) {
            res.status(404).json({ message: error.message, code: error.status, status: "error" });
            next(error);
        }
    };

    public deleteRole = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const roleId = Number(req.params.id);
            const deleteRoleData: Roles = await this.rolesService.deleteRole(roleId);

            res.status(200).json({ data: deleteRoleData, message: 'deleted', status: 'success' });
        } catch (error) {
            res.status(404).json({ message: error.message, code: error.status, status: "error" });
            next(error);
        }
    };
}
export default RolesController;

