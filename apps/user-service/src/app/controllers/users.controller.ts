import { NextFunction, Request, Response } from 'express';
import { User } from '@interfaces/users.interface';
import { CreateUserDto } from '@dtos/users.dto';
import UserService from '../services/users.service';
import {responseCF,bodyCF} from '../../../../../libs/commonResponse/commonResponse'


class UsersController{
    public userService = new UserService();

    public getUsers = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const findAllUsersData: User[] = await this.userService.findAllUser();
            let response = responseCF(
                bodyCF({val: {userData:findAllUsersData}, code: '600',status:true})
              );
              return res.json(response)

            // res.status(200).json({ data: findAllUsersData, message: 'findAll', status: 'success' });
        } catch (error) {
            let response = responseCF(
                bodyCF({err: {message:error.message}, code: '611',status:false})
              );
              return res.json(response)
            // res.status(404).json({ message: error.message, code: error.status, status: "error" });
            next(error);
        }
    };


    public createUser = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const userData: CreateUserDto = req.body;
            const createUserData: User = await this.userService.createUser(userData);

            res.status(201).json({ data: createUserData, message: 'created', status: 'success' });
        } catch (error) {
            res.status(404).json({ message: error.message, code: error.status, status: "error" });
            next(error);
        }
    };

    public updateUser = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const userId = Number(req.params.id);
            const userData: CreateUserDto = req.body;
            const updateUserData: User = await this.userService.updateUser(userId, userData);

            res.status(200).json({ data: updateUserData, message: 'updated', status: 'success' });
        } catch (error) {
            res.status(404).json({ message: error.message, code: error.status, status: "error" });
            console.log(error);
            next(error);
        }
    };
    public deleteUser = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const userId = Number(req.params.id);
            const deleteUserData: User = await this.userService.deleteUser(userId);

            res.status(200).json({ data: deleteUserData, message: 'deleted', status: 'success' });
        } catch (error) {
            res.status(404).json({ message: error.message, code: error.status, status: "error" });
            next(error);
        }
    };

}

export default  UsersController;