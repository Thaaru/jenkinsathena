


import { Router } from "express";
import { Routes } from "@interfaces/routes.interface";
import UsersController from '@controllers/users.controller';



class UsersRoute implements Routes {
    public path = '/';
    public router = Router();
    public usersController = new UsersController();


    constructor() {
        this.initializeRoutes();
    }


    private initializeRoutes() {
        this.router.get(`${this.path}`, this.usersController.getUsers);
        this.router.post(`${this.path}`, this.usersController.createUser);
        this.router.put(`${this.path}/:id(\\d+)`, this.usersController.updateUser);
        this.router.delete(`${this.path}/:id(\\d+)`, this.usersController.deleteUser);  
    }
}
export default UsersRoute;