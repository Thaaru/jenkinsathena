

import express from 'express';
import { NODE_ENV, USERS_PORT, LOG_FORMAT, ORIGIN, CREDENTIALS } from './app/config';
import DB from "../../../libs/shared/database/models/src/index";
import ResponseError from '../../../libs/shared/modules/src/lib/Response/ResponseError';
import { Routes } from './app/interfaces/routes.interface';



class App {
    public app: express.Application;
    public env: string;
    public port: string | number;
    constructor(routes: Routes[]) {
        this.app = express();
        this.env = NODE_ENV || 'development';
        this.port = USERS_PORT || 3000;

        this.connectToDatabase();
        this.initializeMiddlewares()
        this.initializeRoutes(routes);

        }

    public listen() {
        console.log(`Listening at http://localhost:${this.port}/api`);
        this.app.listen(this.port, () => {
            console.log(`Listening Users at http://localhost:${this.port}/api`);

        });

        
    }

    private connectToDatabase() {
        DB.sequelize.sync({ force: false });
    }

    private initializeMiddlewares() {
        // this.app.use(morgan(LOG_FORMAT, { stream }));
        // this.app.use(cors({ origin: ORIGIN, credentials: CREDENTIALS }));
        // // this.app.use(hpp());
        // this.app.use(helmet());
        // this.app.use(compression());
        this.app.use(express.json());
        this.app.use(express.urlencoded({ extended: true }));
        // this.app.use(cookieParser());
    }

    public getServer() {
        return this.app;
    }

    private initializeRoutes(routes: Routes[]) {
        routes.forEach(route => {
            this.app.use('/api/users', route.router);
        });

        this.app.use('*', function (req, res) {
            throw new ResponseError.NotFound(
                `Sorry, HTTP resource you are looking for was not found.`
            )
        })

    }

    




}

export default App;